package com.nityakumari.mainapp.controller;
import java.io.IOException;

import org.jpos.iso.ISOException;
import org.jpos.util.NameRegistrar.NotFoundException;
//import org.jpos.iso.ISOMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.stereotype.Service;
//import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
//import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.nityakumari.mainapp.model.ISOMsgSample;
import com.nityakumari.mainapp.service.ISOContent;
import com.sleepycat.je.utilint.Timestamp;


@Controller
public class JPOSController {

    @Autowired
    @JsonIgnore
    private ISOContent isomessage;
 
    @RequestMapping("/ISO")
    @ResponseBody
    public String welcome() {
        return "Welcome to POS Testing Rest Service change 1.";
    }
 
    @RequestMapping(value = "/ISO/get", //
            method = RequestMethod.GET, //
            produces = { MediaType.APPLICATION_JSON_VALUE,
                     MediaType.APPLICATION_XML_VALUE})
    @ResponseBody
    public String getMessage() throws IOException, ISOException {
    	//System.out.println("Nitya");
    	String isoMsg = (String) isomessage.generateIso();
    	//System.out.println("Nitya1");
    	//System.out.println("Result" +isoMsg);
        return isoMsg;
    }
    @RequestMapping(value = "/ISO/post", //
            method = RequestMethod.POST, //
            produces = { MediaType.APPLICATION_JSON_VALUE, //
                    MediaType.APPLICATION_XML_VALUE })
    @ResponseBody
    public String recieveISO(@RequestBody ISOMsgSample msg) throws IOException, ISOException, NotFoundException {
        //System.out.println("(Service Side) Creating employee: " + msg.getRawData());
    	java.util.Date date= new java.util.Date();
    	System.out.print("Timestamp - " + new Timestamp(date.getTime()));
        System.out.print(" RRN-");
        return isomessage.createMsg(msg);
    }
 
}
