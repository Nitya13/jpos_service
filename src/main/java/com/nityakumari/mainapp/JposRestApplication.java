package com.nityakumari.mainapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//@SpringBootApplication(scanBasePackages={"com.nityakumari.mainapp.controller","com.nityakumari.mainapp.model","com.nityakumari.mainapp.service"})// same as @Configuration @EnableAutoConfiguration @ComponentScan
public class JposRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(JposRestApplication.class, args);
	}
}
