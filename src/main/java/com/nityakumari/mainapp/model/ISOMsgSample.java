package com.nityakumari.mainapp.model;

public class ISOMsgSample {
	private String rawData;
	
	public ISOMsgSample() {
		
	}
	public ISOMsgSample(String rawData) {
		this.rawData = rawData;
		
	}
	public String getRawData() {
		return rawData;
	}
	public void setRawData(String rawData) {
		this.rawData = rawData;
	}

}
