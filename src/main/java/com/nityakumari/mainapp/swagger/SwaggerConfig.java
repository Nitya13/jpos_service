package com.nityakumari.mainapp.swagger;


//import java.util.Collections;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

//import io.swagger.models.Contact;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
	 @Bean	
	 public Docket api() {
		 return new Docket(DocumentationType.SWAGGER_2)  
		          .select()                                  
		          .apis(RequestHandlerSelectors.any())              
		          .paths(PathSelectors.any())                          
		          .build().apiInfo(apiInfo()) ;   
	 }
	 
	 private ApiInfo apiInfo() {
		    return new ApiInfoBuilder()
		            .title("Spring boot swagger2 rest example")
		            .description("Spring boot swagger2 rest example for new users")
		            .termsOfServiceUrl("#")
		            //.contact(new Contact("Code for Social Good", "http://code4socialgood.org/", "info@code4socialgood.org"))
		            .license("Licence")
		            .licenseUrl("#")
		            .version("1.0")
		            .build();
		}

}
