package com.nityakumari.mainapp.service;

import java.io.IOException;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.channel.ASCIIChannel;
//import org.jpos.iso.channel.ChannelPool;
//import org.jpos.iso.ISOUtil;
import org.jpos.iso.packager.GenericPackager;
import org.jpos.util.NameRegistrar.NotFoundException;
//import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Component;

import com.nityakumari.mainapp.model.ISOMsgSample;




@Component
public class ISOContent {
	

	public  String generateIso() throws IOException, ISOException {
		// Create Packager based on XML that contain DE type
		GenericPackager packager = new GenericPackager("basic.xml");
		ISOMsg isoMsg = new ISOMsg();
		isoMsg.setPackager(packager);
		isoMsg.setMTI("1200");
		isoMsg.set(2, "6080320000060501");
		isoMsg.set(3, "001000");
		isoMsg.set(4, "0000000000010000");
		isoMsg.set(11, "882650397121");
		isoMsg.set(12, "20180601193932");
		isoMsg.set(17, "20180601");
		isoMsg.set(24, "200");
		isoMsg.set(32, "900000");
		isoMsg.set(37, "882650397121");
		isoMsg.set(41, "00000000SIAC2354");
		isoMsg.set(42, "SCOTIABANK");
		isoMsg.set(43, "PayU Recharge          Gu~~rgaon      DL");
		isoMsg.set(49, "INR");
		isoMsg.set(59, "CPYBK^6080320000060501^");
		isoMsg.set(102, "01         197     917738798717");
		isoMsg.set(123, "POS");
		isoMsg.set(125, "NFNET");
		System.out.println("Nitya3");
		
		System.out.println(isoMsg);
		byte[] data = isoMsg.pack();
		
		System.out.println("RESULT : " + new String(data));
		String S1 =  new String(data);
		return S1;
		
	
	}
	public String msgGeneration(String SampleMsg) throws IOException, ISOException, NotFoundException{
			String S3 = "";
			try {	
			GenericPackager packager = new GenericPackager("basic.xml");
			ISOMsg isoMsg1 = new ISOMsg();
			isoMsg1.setPackager(packager);
			//System.out.println("Processing Started" + SampleMsg);
			String[] str_array = SampleMsg.split(";");
			System.out.println(str_array[9]);
			isoMsg1.setMTI(str_array[1]);
			for(int i=2;i<=str_array.length;i=i+2) {
				
				
				if(str_array.length == i) {
					//System.out.println("break");
					break;					
				}
				isoMsg1.set(str_array[i], str_array[i+1]);

								
		}

			ASCIIChannel channel = new ASCIIChannel("10.100.9.29",13312,packager);
				channel.connect();
				channel.send(isoMsg1);
				ISOMsg recdMsg = channel.receive();
				S3 = logISOMsg(recdMsg);
				//return S3;
				channel.disconnect();
				
			
        	} catch (IOException e) {
        		System.out.println("IO Block");
            e.printStackTrace();
        	} catch (ISOException e) {
        		System.out.println("ISO Block");
            e.printStackTrace();
        	}finally {
        		
    			//System.out.println("--------------------");
    		}
			return S3;
			
	}	
	
	private static String logISOMsg(ISOMsg recdMsg) {
		StringBuilder result = new StringBuilder();
		try {
					
		result.append("  MTI : ");
		result.append(recdMsg.getMTI());
		result.append(System.getProperty("line.separator"));
		for (int i=1;i<= recdMsg.getMaxField();i++) {
			if (recdMsg.hasField(i)) {
				result.append("Field- "+i+  " :");
				result.append(recdMsg.getString(i));
				result.append(System.getProperty("line.separator"));
				//System.out.println("    Field-"+i+" : "+recdMsg.getString(i));
				}
			}
		
		} catch (ISOException e) {
			//e.printStackTrace();
		} finally {
			//System.out.println("--------------------");
		}
		String result1 = result.toString();
		return result1;
		
	}

	public String createMsg(ISOMsgSample msg) throws IOException, ISOException, NotFoundException {
		String ISOGenerated = "";
		try {
		String msgBreak = msg.getRawData();
		ISOGenerated = msgGeneration(msgBreak);
		} catch (IOException e) {
    		//System.out.println("IO Block--createMsg");
        //e.printStackTrace();
    	} catch (ISOException e) {
    		//System.out.println("ISO Block--createMsg");
        //e.printStackTrace();
    	}finally {
			//System.out.println("--------------------");
		}
		return ISOGenerated;
		
	}

}
